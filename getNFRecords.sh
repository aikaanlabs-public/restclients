#!/bin/bash

########Copyrights AiKaan Labs Pvt Ltd. 2019-20########

# Start of configurations

PartnerURL="partner.aikaan.io"
PartnerLogin="yourlogin"
PartnerPassword="yourpass"

# Name of device, from which you need nf records
nfDeviceName="literally-safe-hippo"

# Start time for netflow records, expressed in nano seconds from epoch
# You could use https://www.epochconverter.com/ for converstion from date
# time to below format

nfStartTime=1561127419000000000

# End time for netflow records, expressed in nano seconds from epoch
nfEndTime=1561131019000000000

# End of configurations

# Prepare the JSON file to login
cat << EOF > data.json
{
	"name": "$PartnerLogin",
	"email": "$PartnerLogin",
	"password": "$PartnerPassword"
}
EOF

# Login to partner and get the JWT, note name is don't care for now
curlArgs="https://$PartnerURL/api/auth/v1/signin -d @data.json"
resp=`curl -s -k $curlArgs `
echo Login response - $resp

userID=`echo $resp | cut -d : -f 5 | cut -d \" -f 2`
jWT=`echo $resp | cut -d : -f 6 | cut -d \" -f 2`
cookieArg=$( printf 'aicon-jwt=%s; userid=%s' "$jWT" "$userID" )
echo Cookies set - $cookieArg

# Get list of devices
curlArgs="https://$PartnerURL/dm/api/dm/v1/devices/$userID?limit=10&offset=0"
echo Call the API - $curlArgs

resp=`curl -sk --cookie "$cookieArg" $curlArgs`
echo API Response - $resp

deviceCount=`echo $resp | jq '.count'`
echo Device count - $deviceCount

# Get the ID of device from which you need to collect the netflow records
for (( index=0; index < deviceCount; index++ ))
do
	deviceName=`echo $resp | jq .devices[$index].name | cut -d \" -f 2 `

	if [ $deviceName = $nfDeviceName ]
		then
		nfDeviceID=`echo $resp | jq .devices[$index].id | cut -d \" -f 2 `
	fi
done

# Get netflow records
cat << EOF > data.json
{
	"deviceid" : "$nfDeviceID",
	"time_start" : $nfStartTime,
        "time_end" :   $nfEndTime
}
EOF

curlArgs="https://$PartnerURL/adam/api/adam/v1/sflow_getter -d @data.json"

# echo curl -sk --cookie $cookieArg $curlArgs

nfRecord=`curl -sk --cookie $cookieArg $curlArgs`
echo $nfRecor | jq
