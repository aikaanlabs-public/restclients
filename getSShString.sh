#!/bin/bash

########Copyrights AiKaan Labs Pvt Ltd. 2019-20########


##############################
# Start of configurations
# replace the below variables 
# correct values

PartnerURL="partner.aikaan.io"
PartnerLogin="yourlogin"
PartnerPassword="yourPassword"

# End of configurations
###############################

help () {
	echo
	echo "$0 <nameOfDevice> "
	echo "returns the ssh connect string for the device"
	exit 1
}


loginToController () {

	# Login to partner and get the JWT, note name is don't care for now
	curlArgs="https://$PartnerURL/api/auth/v1/signin -d @data.json"
	resp=`curl -s -k $curlArgs `

	if [ -z "$resp" ]
	then
		echo Login to controller failed, check login name or password 
		exit 1
	fi

	userID=`echo $resp | cut -d : -f 5 | cut -d \" -f 2`
	jWT=`echo $resp | cut -d : -f 6 | cut -d \" -f 2`
	cookieArg=$( printf 'aicon-jwt=%s;userid=%s' "$jWT" "$userID" )
	#echo Cookies set - $cookieArg
}

getSSHConnect () {
 
	#first form the json data for connect
cat << EOF > ssh.json
{
	"did" : $sshDeviceID,
	"type" : "ssh",
	"lhost" : "localhost",
	"lport" : 22
}
EOF
	#request URL is https://demo.aikaan.io/ps/api/ps/v1/sessions
	curlArgs="https://$PartnerURL/ps/api/ps/v1/sessions -d @ssh.json"

	sshConnectString=`curl -sk --cookie "$cookieArg" $curlArgs`
}


####Main########
# Prepare the JSON file to login
cat << EOF > data.json
{
	"name": "$PartnerLogin",
	"email": "$PartnerLogin",
	"password": "$PartnerPassword"
}
EOF

if [ $# -lt 1 ]
	then
	help
fi

### Check if jq is installed
jqPath=`which jq`

if [ -z $jqPath ]
	then
	echo jq not installed, please try \"sudo apt-get install jq\"
	echo try this after jq is installed
	exit 1

fi

sshDeviceName=$1

# Login into controller
loginToController

# Get list of devices
# ideally, we need to set limit and offset correctly, after getting first entry,
# which will include the total count. For now we are assuming we have max 100
# this need to be fixed later

curlArgs="https://$PartnerURL/dm/api/dm/v1/devices/$userID?limit=100&offset=0"
#echo Call the API - $curlArgs

resp=`curl -sk --cookie "$cookieArg" $curlArgs`
#echo API Response - $resp

deviceCount=`echo $resp | jq '.count'`
#echo Device count - $deviceCount

# Get the ID of device from which you need to get the ssh connect string

for (( index=0; index < deviceCount; index++ ))
do
	deviceName=`echo $resp | jq .devices[$index].name | cut -d \" -f 2 `
#	echo $deviceName
	if [ "$deviceName" = "$sshDeviceName" ]
		then
		#echo getting ssh connect for $deviceName
		theStatus=`echo $resp | jq .devices[$index].status`
		#echo Status is $theStatus
		if [ $theStatus -eq 0 ]
			then
			echo The device is down, hence cannot connect
			exit 1
		fi

		sshDeviceID=`echo $resp | jq .devices[$index].id`
		getSSHConnect
		echo $sshConnectString | jq '.data.commurl' | cut -d \" -f 2
		exit 0
	fi
done

#if came here, no devices were found 
echo $sshDeviceName not found on the controller
exit 1
